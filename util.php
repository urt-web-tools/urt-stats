<?php

require_once('config.php');

function openDB()
{
    global $config;
    global $db_conn;
    $db_conn = mysql_connect($config->db_server, $config->db_username, $config->db_password) or die("Error connecting to mySQL server");
    mysql_select_db($config->db_name, $db_conn) or die("Error selecting mySQL database");
    return true;
}

function closeDB()
{
    global $db_conn;
    if (mysql_close($db_conn)) {
        return true;
    } else {
        return false;
    }
}

?>