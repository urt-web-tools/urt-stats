<?php
define('T_GAME_SEP', '======================================================================================');

$T['team'][TEAM_FFA] = 'NO TEAMS';
$T['team'][TEAM_RED] = 'RED';
$T['team'][TEAM_BLUE] = 'BLUE';
$T['team'][TEAM_SPECTATOR] = 'SPECTATOR';

$T['team_short'][TEAM_FFA] = '-';
$T['team_short'][TEAM_RED] = 'r';
$T['team_short'][TEAM_BLUE] = 'b';
$T['team_short'][TEAM_SPECTATOR] = 's';

//achievements
$T[PACKRAT][NAME] = 'Pack Rat';
$T[PACKRAT][DESC] = 'Hoards all items he encounters.';
$T[CHATSLUT][NAME] = 'Chat Slut';
$T[CHATSLUT][DESC] = 'Talks too much.';
$T[CAPCAPTAIN][NAME] = 'Cap Captain';
$T[CAPCAPTAIN][DESC] = 'Caps more than 5 flags in a match.';
$T[OPPORTUNIST][NAME] = 'Opportunist';
$T[OPPORTUNIST][DESC] = 'Changes team as the wind blows.';
$T[INVISIBLEFLY][NAME] = 'Invisible Fly';
$T[INVISIBLEFLY][DESC] = 'Likes to fly and watch.';
$T[CTFWTF][NAME] = 'CTFWTF';
$T[CTFWTF][DESC] = 'Doesn\'t care about the flag.';

?>