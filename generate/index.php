<?php

require_once('../const.php');
require_once('../config.php');
require_once('../achievements.php');

$col2team = array(
	'red' => TEAM_RED,
	'blue' => TEAM_BLUE);
$otherTeamC = array(
		TEAM_RED => TEAM_BLUE,
		TEAM_BLUE => TEAM_RED);
$otherTeam = array(
		'red' => 'blue',
		'blue' => 'red');
		
global $config;

$game_id = 0;
$last_timestamp;

$state = readState();

function readState() {
    global $config;
    $s = unserialize(file_get_contents(STATE_FILE));
    return $s == FALSE ? array() : $s;
}

function writeState($state) {
    global $config;
    file_put_contents(STATE_FILE, serialize($state));
}

function convertToTimestamp($min, $sec) {
    global $last_timestamp;
    $ts = ($min * 60 + $sec) * 1000;
    if ($ts <= $last_timestamp) {
        $ts = $last_timestamp + 1;
    }
    $last_timestamp = $ts;
    return $ts;
}

function createEvent($timestamp, $type, &$payload) {
    global $game_id;
    $event = array();
    $event['game_id'] = $game_id;
    $event['timestamp'] = $timestamp;
    $event['type'] = $type;
    $event['payload'] = &$payload;
    sanitizeEvent($event);
    return $event;
}

function isWorldKill($weapon) {
    global $WEAPON_WORLD;
   return in_array($weapon, $WEAPON_WORLD);
}

function isWorldHit($attacker_id) {
    global $players;
    return !array_key_exists($attacker_id, $players);
}

echo 'log file = ' . $config->log_file . EOL;

$file = fopen($config->log_file, "r") or exit("Unable to open file!");

if (filesize($config->log_file) == $state['logfilesize']) exit('Stats already generated.');
$startPos = $state['logfilesize'];
$state['logfilesize'] = filesize($config->log_file);

if (file_exists(WORK_FILE)) {
    copy(WORK_FILE, WORK_FILE . '_old.txt') or exit('copying old stats failed.');
    $tmpHandler = fopen(WORK_FILE, 'w');
    fclose($tmpHandler);
}

while (!feof($file))
{
    unset($event);
    unset($payload);
    $line = rtrim(fgets($file));

    if (preg_match('/^\s*(\d+):(\d\d)\s?InitGame:.*\\\g_gametype\\\(\d+)\\\.*\\\mapname\\\(.+?)\\\/', $line, $matches)) {
        $game_id++;
        $last_timestamp = 0;
        $payload['gametype'] = $matches[3];
        $payload['map'] = $matches[4];
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_InitGame, $payload);
    }
    //325:33 InitRound: \sv_allowdownload\0\g_matchmode\0\g_gametype\7\sv_maxclients\24\sv_floodprotect\1\g_warmup\15
    //\capturelimit\0\sv_hostname\Matties UrT ground\g_followstrict\1\fraglimit\0\timelimit\15\g_cahtime\60
    //\g_swaproles\0\g_roundtime\3\g_bombexplodetime\40\g_bombdefusetime\10\g_hotpotato\2\g_waverespawns\1\g_redwave\11
    //\g_bluewave\11\g_respawndelay\8\g_suddendeath\1\g_maxrounds\5\g_friendlyfire\1\g_allowvote\1040187391\g_armbands\0
    //\sv_dlURL\http://u23274:8080/urtmaps\sv_maxPing\0\sv_minPing\0\sv_maxRate\0\sv_minRate\0\dmflags\0
    //\g_maxGameClients\0\g_deadchat\1\g_gear\0\g_teamnamered\rooie\g_teamnameblue\blauwtjes\g_survivorrule\0
    //\version\ioq3 1.35urt win-x86 Dec 20 2007\protocol\68\mapname\ut4_sanc\sv_privateClients\0
    //\ Admin\Mattei\ Email\xxx@xxx.xxx\gamename\q3ut4\g_needpass\0\g_enableDust\0\g_enableBreath\0\g_antilagvis\0
    //\g_survivor\0\g_enablePrecip\0\g_modversion\4.1
    if (preg_match('/^\s*(\d+):(\d\d)\s?InitRound:.*\\\g_gametype\\\(\d+)\\\.*\\\mapname\\\(.+?)\\\/', $line, $matches)) {

        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_InitRound, $payload);
    }

    //0:00 ClientUserinfoChanged: 8 n\-=[UZcl]=-Baksteen\t\1\r\3\tl\0\f0\\f1\\f2\\a0\0\a1\255\a2\0
    if (preg_match('/^\s*(\d+):(\d\d)\s?ClientUserinfoChanged: (\d+)\s?n\\\(.*)\\\t\\\(\d+)\\\r\\\/', $line, $matches)) {
        $payload['player_id'] = $matches[3];
        $payload['player_nick'] = $matches[4];
        $payload['player_team'] = $matches[5];

        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_ClientUserinfoChanged, $payload);
    }

    if (preg_match('/^\s*(\d+):(\d\d)\s?Hit: (\d+) (\d+) (\d+) (\d+): /', $line, $matches)) {
        $payload['victim'] = $matches[3];
        $payload['attacker'] = $matches[4];
        $payload['bodypart'] = $matches[5];
        $payload['weapon'] = $matches[6];
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_Hit, $payload);
        if (isWorldHit($payload['attacker'])) {
            $payload['attacker'] = ATTACKER_WORLD;
        }
    }

    //0 is ook player id!
    // 10:33 Kill: 0 4 39: <non-client> killed figges by UT_MOD_FLAG
// 10:33 Kill: 0 2 39: <non-client> killed Pollux by UT_MOD_FLAG
//  0:24 Kill: 1022 4 6: <world> killed figges by MOD_FALLING

    //  0:42 Kill: 7 3 19: mxup killed Flash by UT_MOD_LR300
    if (preg_match('/^\s*(\d+):(\d\d)\s?Kill: (\d+) (\d+) (\d+): /', $line, $matches)) {
        $payload['attacker'] = $matches[3];
        $payload['victim'] = $matches[4];
        $payload['weapon'] = $matches[5];
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_Kill, $payload);
        if (isWorldKill($payload['weapon'])) {
            $payload['attacker'] = ATTACKER_WORLD;
        }
    }

    //player id is unreliable (0 in spectator mode)
    // 15:07 say: 1 Flash: here kitty kitty
	// 8:26 sayteam: 0 Flash: ja kweeethet
    if (preg_match('/^\s*(\d+):(\d\d)\s?(sayteam|say): (\d+) (.*?): (.*)/', $line, $matches)) {
        $payload['player_id'] = $matches[4];
        $payload['player_nick'] = $matches[5];
        $payload['text'] = $matches[6];
        $payload['teamsay'] = ($matches[3] == 'sayteam');
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_Say, $payload);
    }
	
	
	// CTF events
	//blue flag pickup by player 0
	//	12:37 Item: 0 team_CTF_blueflag
    if (preg_match('/^\s*(\d+):(\d\d)\s?Item: (\d+) team_CTF_(red|blue)flag/', $line, $matches)) {
        $payload['player_id'] = $matches[3];
        $payload['color'] = $matches[4]; // color = flag color
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_CTF_Pickup, $payload);
    } else if (preg_match('/^\s*(\d+):(\d\d)\s?Item: (\d+) (.*)/', $line, $matches)) {
	// item pickup
        $payload['player_id'] = $matches[3];
		$payload['item'] = $matches[4];
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_ItemPickup, $payload);
	}
	
	// blue flag drop by player 1 
	// 62:06 Flag: 1 0: team_CTF_blueflag
    if (preg_match('/^\s*(\d+):(\d\d)\s?Flag: (\d+) (\d+): team_CTF_(red|blue)flag/', $line, $matches)) {
        $payload['player_id'] = $matches[3];
        $flag_action = $matches[4];
        $payload['color'] = $matches[5];
		$event_type = -1;
		switch($flag_action) {
			case 0: $event_type = EVENTTYPE_CTF_Drop; break; //color = flag color
			case 1: $event_type = EVENTTYPE_CTF_Return; break; //color = flag color
			case 2: $event_type = EVENTTYPE_CTF_Cap; break; //color = team that gains points
		}
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), $event_type, $payload);
    }
 
	//red flag returned by itself
	// 64:33 Flag Return: RED
    if (preg_match('/^\s*(\d+):(\d\d)\s?Flag Return: (RED|BLUE)/', $line, $matches)) {
		$payload['color'] = strtolower($matches[3]);
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_CTF_ReturnedByItself, $payload);
	}

    // 15:37 ClientDisconnect: 4
    if (preg_match('/^\s*(\d+):(\d\d)\s?ClientDisconnect: (\d+)/', $line, $matches)) {
        $payload['player_id'] = $matches[3];
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_ClientDisconnect, $payload);
    }

    // 15:27 ShutdownGame:
    if (preg_match('/^\s*(\d+):(\d\d)\s?ShutdownGame:/', $line, $matches)) {
        $event = createEvent(convertToTimestamp($matches[1],$matches[2]), EVENTTYPE_ShutdownGame, $payload);
    }

    if (isset($event)) processEvent($event);

}
fclose($file);

echo 'Add random quote'.EOL;
$report = '======================================================================================'.EOL;
$report .= 'Random mail quote'.EOL;
$report .= '-----------------'.EOL;
$report .= randomMailQuote().EOL;
file_put_contents(WORK_FILE, $report, FILE_APPEND | FILE_TEXT);

//mailStats(file_get_contents(WORK_FILE));

$work_file = fopen(WORK_FILE, 'r');
$output_file = fopen(OUTPUT_FILE, 'w');
copy(WORK_FILE, OUTPUT_FILE) or exit('copying stats failed.');

//writeState($state);
echo 'finished'.EOL;


$players = 0;
$stats = 0;
$achievements = 0;

function sanitizeEvent(&$event) {
    if (isset($event['payload']['player_id'])) $event['payload']['player_id'] = intval($event['payload']['player_id']);
    if (isset($event['payload']['player_team'])) $event['payload']['player_team'] = intval($event['payload']['player_team']);
    if (isset($event['payload']['attacker'])) $event['payload']['attacker'] = intval($event['payload']['attacker']);
    if (isset($event['payload']['victim'])) $event['payload']['victim'] = intval($event['payload']['victim']);
}

function processEvent($event) {
    global $players;
    global $WEAPON_HIT, $WEAPON_KILL, $BODY_PART, $WEAPON_DAMAGE;
    global $stats;
	global $otherTeam;
    global $T;
    switch ($event['type']) {
        case EVENTTYPE_InitGame:
            unset($GLOBALS['stats']);
            unset($GLOBALS['players']);
			unset($GLOBALS['achievements']);
            $GLOBALS['players'][ATTACKER_WORLD] = T_WORLD;
            $GLOBALS['stats']['match']['id'] = $event['game_id'];
            $GLOBALS['stats']['gametype'] = $event['payload']['gametype'];
            $GLOBALS['stats']['match']['map'] = $event['payload']['map'];
            echo $event['timestamp'].'|'.'init game @ '. $event['timestamp'] . ' gametype = '. $event['payload']['gametype'].' map='.$event['payload']['map'].EOL;
            break;
        case EVENTTYPE_InitRound:
            $stats['match']['started'] = true;
            $stats['match']['start_time'] = 0;
            break;
        case EVENTTYPE_ClientUserinfoChanged:
            $player = $event['payload']['player_nick'];
            $team = $event['payload']['player_team'];
            $players[$event['payload']['player_id']] = $player;
            if (!$stats['teams'][$player]['connected']) {
                $stats['teams'][$player]['connected'] = true;
                $stats['teams'][$player]['last_team'] = $team;
                $stats['teams'][$player]['last_timestamp'] = $event['timestamp'];
                //$stats['teams'][$player][$team]['time_spent'] = 0;
            } else {
                if ($team != $stats['teams'][$player]['last_team']) {
                    $stats['teams'][$player][$stats['teams'][$player]['last_team']]['time_spent'] += ($event['timestamp'] - $stats['teams'][$player]['last_timestamp']);
                    $stats['teams'][$player]['last_team'] = $team;
                    $stats['teams'][$player]['last_timestamp'] = $event['timestamp'];
                }
            }
            echo $event['timestamp'].'|'.'player info changed id='. $event['payload']['player_id'] . ' = '. $event['payload']['player_nick'].' (team='.$T['team'][$team].')'.EOL;
            break;
        case EVENTTYPE_Hit:
            $attacker = $players[$event['payload']['attacker']];
            $victim = $players[$event['payload']['victim']];
            $stats['hits'][$attacker][$victim]++;
            $stats['hits'][$attacker]['totalhits']++;
            if ($event['payload']['bodypart'] == BODY_PART_HEAD || $event['payload']['bodypart'] == BODY_PART_HELMET) {
                $stats['hits'][$attacker]['totalheadhits']++;
            }
            $damage = $WEAPON_DAMAGE[$event['payload']['weapon']][$event['payload']['bodypart']];
            $stats['damage'][$attacker][$victim] += $damage;
            $stats['totaldamage'][$attacker]['dealt'] += $damage;
            $stats['totaldamage'][$victim]['received'] += $damage;
            echo $event['timestamp'].'|'.$players[$event['payload']['attacker']] . ' hits ' . $players[$event['payload']['victim']] . ' with ' . $WEAPON_HIT[$event['payload']['weapon']] . ' in the ' . $BODY_PART[$event['payload']['bodypart']].' (dmg='.$damage.')'.EOL;
            break;
        case EVENTTYPE_Kill:
            $attacker = $players[$event['payload']['attacker']];
            $victim = $players[$event['payload']['victim']];
            if ($event['payload']['weapon'] == WEAPON_KILL_Changing_Team) {
                $stats['teamchanges'][$attacker]++;
                echo $event['timestamp'].'|'.$attacker . ' changes team'.EOL;
            } else {
                $stats['kills'][$attacker][$victim]++;
                $stats['killdeathratio'][$attacker]['kills']++;
                $stats['killdeathratio'][$victim]['deaths']++;
                echo $event['timestamp'].'|'.$attacker . ' kills ' . $victim . ' with ' . $WEAPON_KILL[$event['payload']['weapon']].EOL;
            }
            break;
        case EVENTTYPE_Say:
			$teamsay = $event['payload']['teamsay'];
			if (!$teamsay) {
				$stats['chat'][$event['payload']['player_nick']][] = $event['payload']['text'];
			}
			$stats['chatbox'][] = ($teamsay ? '>' : ' ' ) . $T['team_short'][$stats['teams'][$event['payload']['player_nick']]['last_team']] . ' ' . $event['payload']['player_nick']. ': '.$event['payload']['text'];
            echo $event['timestamp'].'|'.$event['payload']['player_nick']. ' says '.$event['payload']['text']. ($teamsay == TRUE ? ' (teamsay)' : '') .EOL;
            break;
		case EVENTTYPE_ItemPickup:
			$player = $players[$event['payload']['player_id']];
			$stats[ACHIEVEMENTS]['packrat'][$player]++;
			echo $event['timestamp'].'|'.$player. ' picks up item '.$event['payload']['item'].EOL;
			break;
		case EVENTTYPE_CTF_Pickup:
			$player = $players[$event['payload']['player_id']];
			$color = $event['payload']['color']; // flag color
			if ($stats['ctf']['teams'][$color]['flagtaken']) {
				$stats['ctf']['players'][$player]['pickups']++;
				$stats['ctf']['teams'][$otherTeam[$color]]['pickups']++;
				echo $event['timestamp'].'|'.$player. ' picks up the '.$color.' flag'.EOL;
			} else {
				$stats['ctf']['players'][$player]['steals']++;
				$stats['ctf']['teams'][$otherTeam[$color]]['steals']++;
				$stats['ctf']['teams'][$color]['flagtaken'] = true;
				echo $event['timestamp'].'|'.$player. ' steals the '.$color.' flag'.EOL;
			}
			break;
		case EVENTTYPE_CTF_Drop:
			$player = $players[$event['payload']['player_id']];
			$color = $event['payload']['color']; // flag color
			$stats['ctf']['players'][$player]['drops']++;
			$stats['ctf']['teams'][$otherTeam[$color]]['drops']++;
			echo $event['timestamp'].'|'.$player. ' drops the '.$color.' flag'.EOL;
			break;
		case EVENTTYPE_CTF_Return:
			$player = $players[$event['payload']['player_id']];
			$color = $event['payload']['color']; // flag color
			$stats['ctf']['players'][$player]['returns']++;
			$stats['ctf']['teams'][$color]['returns']++;
			$stats['ctf']['teams'][$color]['flagtaken'] = false;
			echo $event['timestamp'].'|'.$player. ' returns the '.$color.' flag'.EOL;
			break;
		case EVENTTYPE_CTF_ReturnedByItself:
			$color = $event['payload']['color']; // flag color
			$stats['ctf']['teams'][$color]['abandoned']++;
			$stats['ctf']['teams'][$color]['flagtaken'] = false;
			echo $event['timestamp'].'|'.$color. ' team abandoned the flag'.EOL;
			break;
		case EVENTTYPE_CTF_Cap:
			$player = $players[$event['payload']['player_id']];
			$color = $event['payload']['color']; // team gaining points
			$stats['ctf']['players'][$player]['caps']++;
			$stats['ctf']['teams'][$color]['caps']++;
			$stats['ctf']['teams'][$otherTeam[$color]]['flagtaken'] = false;
			echo $event['timestamp'].'|'.$player. ' makes a cap for the '.$color.' team'.EOL;
			break;
		
        case EVENTTYPE_ClientDisconnect:
            $player = $players[$event['payload']['player_id']];
            $stats['teams'][$player]['connected'] = false;
            $stats['teams'][$player][$stats['teams'][$player]['last_team']]['time_spent'] += ($event['timestamp'] - $stats['teams'][$player]['last_timestamp']);
            $mins = ($stats['teams'][$player][TEAM_RED]['time_spent'] + $stats['teams'][$player][TEAM_BLUE]['time_spent']) / (60000);
            echo $event['timestamp'].'|'.$player. ' disconnects after '.$mins.' minutes'.EOL;
            break;
        case EVENTTYPE_ShutdownGame:
            $stats['match']['stop_time'] = $event['timestamp'];
            calcStats();
            printStats();
            break;
        default:
            echo 'unhandled event '.$event['type'].EOL;
    }
}

function frand() {
    return mt_rand()/mt_getrandmax();
}

function randomElem($arr) {
    return $arr[(count($arr) - 1) * frand()];
}

//todo replace with closure (php 5.3+)
function sortNem($a, $b) {
	return (reset($a) > reset($b)) ? -1 : 1;
}

function calcAchievements() {
	global $stats;
	global $achievements;
	global $achievement_thresholds;
	
	foreach ($stats[ACHIEVEMENTS] as $achievement => $candidates) {
		arsort($candidates, SORT_NUMERIC);
		reset($candidates);
		$candidate = each($candidates);
		echo 'achievement candidate for "'.$achievement.'": ' . $candidate['key'] .' ('. $candidate['value'].'>?='.$achievement_thresholds[$achievement].') ';
		//echo 'achievement '. $achievement.' has threshold '. $achievement_thresholds[$achievement].EOL;
		if ($candidate['value'] >= $achievement_thresholds[$achievement]) {
			$achievements[$achievement] = $candidate;
			echo 'SUCCESS'.EOL;
		} else {
			echo 'FAIL'.EOL;
		}

	}
}

function calcStats() {
    global $stats;
	global $achievements;
	global $players;
    
    foreach ($stats['chat'] as $playername => $playerchat) {
//        echo 'index = '.count($playerchat) * (mt_rand()/mt_getrandmax()).EOL;
        $stats['randomchat'][$playername] = $playerchat[(count($playerchat)-1) * frand()];
		$stats[ACHIEVEMENTS][CHATSLUT][$playername] = count($playerchat);
        if ($stats['mostchat']['number'] < count($playerchat)) {
            $stats['mostchat']['player'] = $playername;
            $stats['mostchat']['number'] = count($playerchat);
        }
    }
    $kdr = array();
    foreach ($stats['killdeathratio'] as $playername => &$kdstats) {
		$kdstats['kdr'] = $kdstats['kills']/$kdstats['deaths'];
		$kdr[$playername] = $kdstats['kdr'];
		if ($kdstats['deaths'] == 0) {
			$kdr[$playername] = 'flawless (' . $kdstats['kills'] . ')';
		}
    }
    array_multisort($kdr, SORT_DESC, $stats['killdeathratio']);

    $hsr = array();
    foreach ($stats['hits'] as $playername => &$hitstats) {
        $hitstats['headshotperc'] = $hitstats['totalheadhits'] /$hitstats['totalhits'];
        $hsr[$playername] = $hitstats['headshotperc'];
    }
    array_multisort($hsr, SORT_DESC, $stats['hits']);

    $dmg = array();
    foreach ($stats['totaldamage'] as $playername => &$dmgstats) {
        $dmg[$playername] = $dmgstats['dealt'];
    }
    array_multisort($dmg, SORT_DESC, $stats['totaldamage']);

	//nemesis
	foreach ($stats['kills'] as $attacker => &$victim) {
		foreach ($victim as $vname => $kills) {
			$stats['nemesis'][$vname][$attacker] = $kills;
		}
	}
	foreach ($stats['nemesis'] as $victim => &$attacker) {
		arsort($attacker, SORT_NUMERIC);
	}
	uasort($stats['nemesis'], 'sortNem');

	//calculate in which team a player spent most of his time
    foreach ($stats['teams'] as $playername => &$teamstat) {
        if ($teamstat['connected']) {
            $teamstat[$teamstat['last_team']]['time_spent'] += ($stats['match']['stop_time'] - $teamstat['last_timestamp']);
        }
        if ($teamstat[TEAM_RED]['time_spent'] > $teamstat[TEAM_BLUE]['time_spent']){
            $teamstat['dominant_team'] = TEAM_RED;
        } else {
            $teamstat['dominant_team'] = TEAM_BLUE;
        }
        $teamstat['total_time'] = $teamstat[TEAM_RED]['time_spent'] + $teamstat[TEAM_BLUE]['time_spent'] + $teamstat[TEAM_SPECTATOR]['time_spent'];
    }
	
	//ctf
	

	// achievement: opportunist
	$stats[ACHIEVEMENTS][OPPORTUNIST] = &$stats['teamchanges'];
	
	// achievement: invisiblefly
	foreach ($stats['teams'] as $player => $teaminfo) {
		if (array_key_exists(TEAM_SPECTATOR, $teaminfo)) {
			$stats[ACHIEVEMENTS][INVISIBLEFLY][$player] = $teaminfo[TEAM_SPECTATOR]['time_spent'];
		}
	}

	// achievement: CTFWTF
	foreach ($stats['teams'] as $playername => $playerstat) {
		//todo empty check werkt precies nog niet te goei
		if (empty($stats['ctf'][$playername])) {
			$stats[ACHIEVEMENTS][CTFWTF][$playername] = $playerstat['total_time'];
		}
	}
	
	calcAchievements();

    return $stats;
}
function pivotArray($arr1) {
	$teams = array();
	$statnames = array();
	$arr2 = array();
	foreach ($arr1 as $team => $stat) {
		$teams[] = $team;
		$statnames = array_merge($statnames, array_keys($stat));
	}
	$statnames = array_unique($statnames);
	foreach ($statnames as $statname) {
		foreach ($teams as $team) {
			$arr2[$statname][$team] = $arr1[$team][$statname];
		}
	}
	return $arr2;
}

function printStats() {
    global $stats;
	global $achievements;
    global $config;
	global $T;
    $report[] = T_GAME_SEP.EOL;
    $report[] = 'STATS FOR GAME '.$stats['match']['id'].' - '.$stats['match']['map'].EOL.EOL;

    $report[] = 'Teams'.EOL;
    $report[] = '-----'.EOL;
    $report[] = "                      RED  BLU SPEC".EOL;
    foreach ($stats['teams'] as $playername => $teamstat) {
        $report[] = sprintf("%'.-20s %4d %4d %4d".EOL,$playername, $teamstat[TEAM_RED]['time_spent'] / 60000, $teamstat[TEAM_BLUE]['time_spent'] / 60000, $teamstat[TEAM_SPECTATOR]['time_spent'] / 60000);
    }
    $report[] = EOL;


    unset($stats['kills'][T_WORLD]);
    unset($stats['killdeathratio'][T_WORLD]);
    $report[] = 'Kill/death ratio'.EOL;
    $report[] = '----------------'.EOL;
    foreach ($stats['killdeathratio'] as $playername => $kdstats) {
        $report[] = sprintf("%s %'.-20s %s".EOL, $T['team_short'][$stats['teams'][$playername]['dominant_team']], $playername, number_format($kdstats['kills']/$kdstats['deaths'], 2).' ('.$kdstats['kills'].'/'.$kdstats['deaths'].')');
    }
    $report[] = EOL;

    $report[] = 'Head shot rate'.EOL;
    $report[] = '--------------'.EOL;
    foreach ($stats['hits'] as $playername => $hitstats) {
        $report[] = sprintf("%'.-20s %5.2f %% (%d/%d)".EOL,$playername, number_format($hitstats['headshotperc']*100.0, 2), $hitstats['totalheadhits'], $hitstats['totalhits']);
    }
    $report[] = EOL;

    $report[] = 'Damage'.EOL;
    $report[] = '------'.EOL;
    $report[] = "                     dealt  rcvd  rate".EOL;
    foreach ($stats['totaldamage'] as $playername => $damagestats) {
        $report[] = sprintf("%'.-20s %5d %5d %5.2f".EOL,$playername, $damagestats['dealt'], $damagestats['received'], number_format($damagestats['dealt'] / $damagestats['received'], 2));
    }
    $report[] = EOL;

    $report[] = 'Nemesis'.EOL;
    $report[] = '-------'.EOL;
    $report[] = "player               nemesis              kills".EOL;
    foreach ($stats['nemesis'] as $victim => &$attackers) {
        $report[] = sprintf("%'.-20s %'.-20s %3d".EOL, $victim, key($attackers), current($attackers));
	}
    $report[] = EOL;
	
    $report[] = 'CTF stats'.EOL;
    $report[] = '========='.EOL;
    $report[] = 'Team stats'.EOL;
    $report[] = '----------'.EOL;
    $report[] = "            RED  BLU".EOL;
	foreach (pivotArray($stats['ctf']['teams']) as $statname => $teamstat) {
		$report[] = sprintf("%'.-10s %4d %4d".EOL,$statname, $teamstat['red'], $teamstat['blue']);
	}
    $report[] = EOL;
	
    $report[] = 'Player stats'.EOL;
    $report[] = '------------'.EOL;
//	print_r(pivotArray($stats['ctf']['players']));
	foreach (pivotArray($stats['ctf']['players']) as $statname => $playerstat) {
		arsort($playerstat, SORT_NUMERIC);
		$winner = each($playerstat);
		$report[] = sprintf("%'.-10s %'.-20s %4d".EOL, $statname, $winner['key'], $winner['value']);
	}
    $report[] = EOL;

    $report[] = 'Achievements'.EOL;
    $report[] = '------------'.EOL;
	if (empty($achievements)) {
		$report[] = 'no heroical actions recorded.'.EOL;
	} else {
		foreach ($achievements as $achievement => $winner) {
			$report[] = sprintf("%'.-40s %'.-20s %3d".EOL, $T[$achievement][NAME],$winner['key'].' ('.$winner['value'].')');		
		}
	}
    $report[] = EOL;
    $report[] = 'Achievements guide: <url>'.EOL;
    $report[] = EOL;
	

    $report[] = 'Random comments on this game'.EOL;
    $report[] = '----------------------------'.EOL;
    foreach ($stats['randomchat'] as $playername => $line) {
        $report[] = '- '.$playername.': '.$line.EOL;
    }
    $report[] = 'Most chatty person: '.$stats['mostchat']['player'].' ('.$stats['mostchat']['number'].')'.EOL;
    $report[] = EOL;

    $report[] = 'Full chatlog: '.MX_WEBROOT.CHATLOG_PATH.$stats['match']['id'].'.txt'.EOL;
    $report[] = EOL;
    $report[] = EOL;

    global $file;
    global $startPos;
    if (ftell($file) > $startPos) file_put_contents(WORK_FILE, $report, FILE_APPEND | FILE_TEXT);
	file_put_contents(MX_PATH . CHATLOG_PATH.$stats['match']['id'].'.txt', implode(EOL, $stats['chatbox']), FILE_TEXT);
}

function randomMailQuote() {
    global $config;
    if (empty($config->mailquotes_file)) return;
    $file = fopen($config->mailquotes_file, "r") or die('error opening mailquotes file');
    $line = trim(fgets($file));
    $quotes = array();
    while (!feof($file)) {
        $quote = array();
        $quote['date'] = $line;
        $line = trim(fgets($file));
        while (!empty($line)) {
            $quote['lines'][] = $line;
            $line = trim(fgets($file));
        }
        $quotes[] = $quote;
        $line = trim(fgets($file));
    }
    fclose($file);
//    print_r($quotes);
    $randquote = randomElem($quotes);
    return implode(EOL, $randquote['lines']);
}

function mailStats($report) {
    global $config;
    if (empty($config->mail_to)) return;
    $today = getdate();
    $subj = 'Urt stats ' . $today['mday'] . '/' . $today['mon'];
    echo 'Send mail'.EOL;
    mail($config->mail_to, $subj, $report, $config->mail_from);
}
?>